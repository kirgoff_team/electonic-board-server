package main

import (
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/database"
)

func main()  {
	database.Open()
	defer database.Close()

	proto_server.Start()
}