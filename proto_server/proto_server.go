package proto_server

import (
	"crypto/tls"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/v1"
)

const (
	certPath = "device_crt/device.crt"
	keyPath  = "device_crt/device.key"
	address  = ":3443"
)

func Start() {

	router := mux.NewRouter().StrictSlash(true)

	apiRouterV1 := router.
		Headers("Content-Type", "application/json").
		PathPrefix("/api_v1").
		Subrouter()

	apiRouterV1User := apiRouterV1.PathPrefix("/user").Subrouter()
	apiRouterV1Users := apiRouterV1.PathPrefix("/users").Subrouter()
	apiRouterV1Server := apiRouterV1.PathPrefix("/server").Subrouter()

	apiRouterV1.HandleFunc("/data/{something}", api_v1.Hello).Methods("GET")

	apiRouterV1User.HandleFunc("/sign_up", api_v1.SignUp).Methods("POST")
	apiRouterV1User.HandleFunc("/sign_in", api_v1.SignIn).Methods("POST")
	apiRouterV1User.HandleFunc("/change_login", api_v1.ChangeLogin).Methods("PUT")
	apiRouterV1User.HandleFunc("/change_password", api_v1.ChangePassword).Methods("PUT")

	apiRouterV1Users.HandleFunc("/", api_v1.GetUsers).Methods("GET")
	apiRouterV1Users.HandleFunc("/{user_id}", api_v1.GetUserInfo).Methods("GET")
	apiRouterV1Users.HandleFunc("/{user_id}", api_v1.DeleteUser).Methods("DELETE")
	apiRouterV1Users.HandleFunc("/{user_id}/change_user_type", api_v1.ChangeUserType).Methods("PUT")

	apiRouterV1Server.HandleFunc("/version", api_v1.Version).Methods("GET")

	// TLS Configuration
	cfg := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}

	// Create server
	srv := &http.Server{
		Addr:         address,
		Handler:      apiRouterV1,
		TLSConfig:    cfg,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
	}

	// Start server to listen
	log.Fatal(srv.ListenAndServeTLS(certPath, keyPath))
}
