package token

import (
	"github.com/dgrijalva/jwt-go"
	"time"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api"
)

// Recommended size for a secret key is 64 bytes
const secretKey = "dClsmGv2MFkC3tH71P3M4b2nDdUmhJhGaQb2jkZp5XKAjxZJa6L58IxLrC6wokFR"

type AccessTokenClaims struct {
	UserId string `json:"user_id"`
	jwt.StandardClaims
}

func GenerateAccessToken(userId string) (string, error) {

	claims := AccessTokenClaims{
		userId,
		jwt.StandardClaims{
			ExpiresAt: time.Now().AddDate(1, 0, 0).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, err := token.SignedString([]byte(secretKey))

	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func ParseAccessToken(tokenString string) (*jwt.Token, error) {

	if tokenString == "" {
		return nil, &api.ServerError{Code: api.ErrBadToken}
	}

	tokenJwt, err := jwt.ParseWithClaims(tokenString, &AccessTokenClaims{}, func(tokenJwt *jwt.Token) (interface{}, error) {
		// validate the algorithm
		if _, ok := tokenJwt.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, &api.ServerError{Code: api.ErrBadToken}
		}

		return []byte(secretKey), nil
	})

	return tokenJwt, err
}

func GetUserIdFromAccessToken(tokenString string) (userId string, err error)  {
	tokenJwt, err := ParseAccessToken(tokenString)

	if err != nil {
		return "", err
	}

	if claims, ok := tokenJwt.Claims.(*AccessTokenClaims); ok && tokenJwt.Valid {
		userId = claims.UserId
		return userId, nil
	} else {
		return "", &api.ServerError{Code: api.ErrBadToken}
	}
}