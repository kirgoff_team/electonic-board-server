package api

import (
	"net/http"
	"encoding/json"
	"strconv"
)

func RespondWithError(w http.ResponseWriter, httpCode int, errorCode ErrorCode) {
	RespondWithJSON(w, httpCode, map[string]string{"code":strconv.Itoa(int(errorCode)), "error": errorCode.String()})
}

func RespondWithInternalError(w http.ResponseWriter, message string) {
	RespondWithJSON(w, http.StatusInternalServerError, map[string]string{"code":strconv.Itoa(int(ErrInternal)), "error": message})
}

func RespondWithJSON(w http.ResponseWriter, httpCode int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpCode)
	w.Write(response)
}

func RespondWithEmptyJSON(w http.ResponseWriter, httpCode int) {
	RespondWithJSON(w, httpCode, map[string]interface{}{})
}