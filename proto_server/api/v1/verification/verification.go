package verification

const (
	minLoginLength = 4
	minPasswordLength = 4
)

func LoginIsTooShort(login string) bool {
	return stringLengthIsTooShort(login, minLoginLength)
}

func PasswordIsTooShort(login string) bool {
	return stringLengthIsTooShort(login, minPasswordLength)
}

func stringLengthIsTooShort(string string, minLength int) bool {
	if len(string) < minLength {
		return true
	}

	return false
}

func GetMinLoginLength() int {
	return minLoginLength
}

func GetMinPasswordLength() int {
	return minPasswordLength
}