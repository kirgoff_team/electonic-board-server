package api_v1

import (
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/token"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/v1/data"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/database"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/utils"
	"encoding/json"
	"net/http"
)

type SignInRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type SignInResponse struct {
	User data.User `json:"user"`
	Jwt  string    `json:"jwt"`
}

// Authorization
func SignIn(w http.ResponseWriter, req *http.Request) {
	// decode request body
	decoder := json.NewDecoder(req.Body)

	var signInRequest SignInRequest
	if err := decoder.Decode(&signInRequest); err != nil {
		api.RespondWithError(w, http.StatusBadRequest, api.ErrInvalidRequestPayload)
		return
	}

	defer req.Body.Close()

	// get user
	user, err := database.GetUser(signInRequest.Login, signInRequest.Password)
	if err != nil {
		serverErr, ok := err.(*api.ServerError)

		if ok {
			if serverErr.Code == api.ErrAuthorization {
				api.RespondWithError(w, http.StatusUnprocessableEntity, api.ErrAuthorization)
				return
			} else {
				api.RespondWithInternalError(w, err.Error())
				return
			}
		} else {
			api.RespondWithInternalError(w, err.Error())
			return
		}
	}

	// create new jwt token
	accessToken, err := token.GenerateAccessToken(user.ID.String())
	if err != nil {
		api.RespondWithInternalError(w, err.Error())
		return
	}

	// create Response
	signInResponse := SignInResponse{
		data.User{
			Id:                 user.ID.String(),
			Login:              user.Login,
			UserType:           int(user.UserType),
			MustChangePassword: utils.Itob(user.MustChangePassword)},
		accessToken}

	api.RespondWithJSON(w, http.StatusOK, signInResponse)
}