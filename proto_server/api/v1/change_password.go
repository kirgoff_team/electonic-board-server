package api_v1

import (
	"net/http"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/v1/data"
	"encoding/json"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/token"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/v1/verification"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/database"
)

type ChangePasswordRequest struct {
	Password string `json:"password"`
}

type ChangePasswordResponse struct {
	User data.User `json:"user"`
	Jwt  string    `json:"jwt"`
}

// Change password
func ChangePassword(w http.ResponseWriter, req *http.Request) {
	// decode request body
	decoder := json.NewDecoder(req.Body)

	var changePasswordRequest ChangePasswordRequest
	if err := decoder.Decode(&changePasswordRequest); err != nil {
		api.RespondWithError(w, http.StatusBadRequest, api.ErrInvalidRequestPayload)
		return
	}

	defer req.Body.Close()

	// get user id from token
	tokenString := req.Header.Get("Authorization")
	userId, err := token.GetUserIdFromAccessToken(tokenString)
	if err != nil {
		api.RespondWithError(w, http.StatusUnauthorized, api.ErrBadToken)
		return
	}

	// password verification
	if verification.PasswordIsTooShort(changePasswordRequest.Password) {
		api.RespondWithError(w, http.StatusUnprocessableEntity, api.ErrPasswordIsTooShort)
		return
	}

	// update password
	user, err := database.ChangePassword(userId, changePasswordRequest.Password)

	if err != nil {
		serverErr, ok := err.(*api.ServerError)
		if ok {
			switch serverErr.Code {
			case api.ErrUserNotFound:
				api.RespondWithError(w, http.StatusNotFound, api.ErrRequestingUserNotFound)
				return
			}
		}

		api.RespondWithInternalError(w, err.Error())
		return
	}

	// create new jwt token
	accessToken, err := token.GenerateAccessToken(user.ID.String())
	if err != nil {
		api.RespondWithInternalError(w, err.Error())
		return
	}

	// create Response
	changePasswordResponse := ChangePasswordResponse{
		data.User{
			Id:                 user.ID.String(),
			Login:              user.Login,
			UserType:           int(user.UserType),
			MustChangePassword: false},
		accessToken}

	api.RespondWithJSON(w, http.StatusOK, changePasswordResponse)
}