package api_v1

import (
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/token"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/database"
	"github.com/gorilla/mux"
	"net/http"
)

type UserInfoResponse struct {
	Id       string `json:"id"`
	Login    string `json:"login"`
	UserType int    `json:"user_type"`
}

// Get User Info
func GetUserInfo(w http.ResponseWriter, req *http.Request) {

	// get user id from token
	tokenString := req.Header.Get("Authorization")
	userId, err := token.GetUserIdFromAccessToken(tokenString)
	if err != nil {
		api.RespondWithError(w, http.StatusUnauthorized, api.ErrBadToken)
		return
	}

	// check user rights
	userType, err := database.GetUserType(userId)
	if err != nil {
		serverErr, ok := err.(*api.ServerError)
		if ok {
			switch serverErr.Code {
			case api.ErrUserNotFound:
				api.RespondWithError(w, http.StatusNotFound, api.ErrRequestingUserNotFound)
				return
			}
		}

		api.RespondWithInternalError(w, err.Error())
		return
	}

	if (database.Admin & userType) == 0 {
		api.RespondWithError(w, http.StatusForbidden, api.ErrPermissionDenied)
		return
	}

	// get requested user id
	vars := mux.Vars(req)
	requestedUserId := vars["user_id"]

	// get user info from database
	requestedUser, err := database.GetUserById(requestedUserId)
	if err != nil {
		serverErr, ok := err.(*api.ServerError)
		if ok {
			switch serverErr.Code {
			case api.ErrUserNotFound:
				api.RespondWithError(w, http.StatusNotFound, api.ErrUserNotFound)
				return
			}
		}

		api.RespondWithInternalError(w, err.Error())
		return
	}

	// create response
	userInfoResponse := UserInfoResponse{
		Id:       requestedUser.ID.String(),
		Login:    requestedUser.Login,
		UserType: int(requestedUser.UserType)}

	api.RespondWithJSON(w, http.StatusOK, userInfoResponse)
}
