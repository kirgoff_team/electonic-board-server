package api_v1

import (
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/token"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/v1/data"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/v1/verification"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/database"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/utils"
	"encoding/json"
	"net/http"
)

type ChangeLoginRequest struct {
	Login string `json:"login"`
}

type ChangeLoginResponse struct {
	User data.User `json:"user"`
	Jwt  string    `json:"jwt"`
}

// Change login
func ChangeLogin(w http.ResponseWriter, req *http.Request) {
	// decode request body
	decoder := json.NewDecoder(req.Body)

	var changeLoginRequest ChangeLoginRequest
	if err := decoder.Decode(&changeLoginRequest); err != nil {
		api.RespondWithError(w, http.StatusBadRequest, api.ErrInvalidRequestPayload)
		return
	}

	defer req.Body.Close()

	// get user id from token
	tokenString := req.Header.Get("Authorization")
	userId, err := token.GetUserIdFromAccessToken(tokenString)
	if err != nil {
		api.RespondWithError(w, http.StatusUnauthorized, api.ErrBadToken)
		return
	}

	// login verification
	if verification.LoginIsTooShort(changeLoginRequest.Login) {
		api.RespondWithError(w, http.StatusUnprocessableEntity, api.ErrLoginIsTooShort)
		return
	}

	// update login
	user, err := database.ChangeLogin(userId, changeLoginRequest.Login)
	if err != nil {
		serverErr, ok := err.(*api.ServerError)
		if ok {
			switch serverErr.Code {
			case api.ErrUserNotFound:
				api.RespondWithError(w, http.StatusNotFound, api.ErrRequestingUserNotFound)
				return
			case api.ErrLoginIsAlreadyInUse:
				api.RespondWithError(w, http.StatusUnprocessableEntity, api.ErrLoginIsAlreadyInUse)
				return
			}
		}

		api.RespondWithInternalError(w, err.Error())
		return
	}

	// create new jwt token
	accessToken, err := token.GenerateAccessToken(user.ID.String())
	if err != nil {
		api.RespondWithInternalError(w, err.Error())
		return
	}

	// create Response
	changeLoginResponse := ChangeLoginResponse{
		data.User{
			Id:                 user.ID.String(),
			Login:              user.Login,
			UserType:           int(user.UserType),
			MustChangePassword: utils.Itob(user.MustChangePassword)},
		accessToken}

	api.RespondWithJSON(w, http.StatusOK, changeLoginResponse)
}
