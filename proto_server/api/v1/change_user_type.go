package api_v1

import (
	"net/http"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/database"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/token"
	"github.com/gorilla/mux"
	"encoding/json"
)

type ChangeUserTypeRequest struct {
	UserType int `json:"user_type"`
}

type ChangeUserTypeResponse struct {
	Id       string `json:"id"`
	Login    string `json:"login"`
	UserType int    `json:"user_type"`
}

// Change User Type
func ChangeUserType(w http.ResponseWriter, req *http.Request) {
	// decode request body
	decoder := json.NewDecoder(req.Body)

	var changeUserTypeRequest ChangeUserTypeRequest
	if err := decoder.Decode(&changeUserTypeRequest); err != nil {
		api.RespondWithError(w, http.StatusBadRequest, api.ErrInvalidRequestPayload)
		return
	}

	defer req.Body.Close()


	// check new UserType
	requestedUserType := database.UserType(changeUserTypeRequest.UserType)
	if  requestedUserType.TypeIsUnknown() {
		api.RespondWithError(w, http.StatusBadRequest, api.ErrUserTypeIsUnknown)
		return
	}


	// get user id from token
	tokenString := req.Header.Get("Authorization")
	userId, err := token.GetUserIdFromAccessToken(tokenString)
	if err != nil {
		api.RespondWithError(w, http.StatusUnauthorized, api.ErrBadToken)
		return
	}

	// check user rights
	userType, err := database.GetUserType(userId)
	if err != nil {
		serverErr, ok := err.(*api.ServerError)
		if ok {
			switch serverErr.Code {
			case api.ErrUserNotFound:
				api.RespondWithError(w, http.StatusNotFound, api.ErrRequestingUserNotFound)
				return
			}
		}

		api.RespondWithInternalError(w, err.Error())
		return
	}

	if (database.Admin & userType) == 0 {
		api.RespondWithError(w, http.StatusForbidden, api.ErrPermissionDenied)
		return
	}

	// get requested user id
	vars := mux.Vars(req)
	requestedUserId := vars["user_id"]

	// change UserType in database
	requestedUser, err := database.ChangeUserType(requestedUserId, requestedUserType)
	if err != nil {
		serverErr, ok := err.(*api.ServerError)
		if ok {
			switch serverErr.Code {
			case api.ErrUserNotFound:
				api.RespondWithError(w, http.StatusNotFound, api.ErrUserNotFound)
				return
			}
		}

		api.RespondWithInternalError(w, err.Error())
		return
	}

	// create Response
	changeUserTypeResponse := ChangeUserTypeResponse{
		Id:       requestedUser.ID.String(),
		Login:    requestedUser.Login,
		UserType: int(requestedUser.UserType)}

	api.RespondWithJSON(w, http.StatusOK, changeUserTypeResponse)
}