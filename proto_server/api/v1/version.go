package api_v1

import (
	"net/http"

	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/config"
)

type VersionResponse struct {
	Version	string	`json:"version"`
}

// Get Proto Server Version
func Version(w http.ResponseWriter, req *http.Request) {
	api.RespondWithJSON(w, http.StatusOK, VersionResponse{Version: config.VersionName})
}