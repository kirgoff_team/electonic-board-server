package data

type User struct {
	Id                 string `json:"id"`
	Login              string `json:"login"`
	UserType 		   int    `json:"user_type"`
	MustChangePassword bool   `json:"must_change_password"`
}