package api_v1

import (
	"net/http"
	"encoding/json"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/database"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/token"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/v1/data"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/utils"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/v1/verification"
)

type SignUpRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type SignUpResponse struct {
	User data.User `json:"user"`
	Jwt  string    `json:"jwt"`
}

// Registration
func SignUp(w http.ResponseWriter, req *http.Request) {

	// decode request body
	decoder := json.NewDecoder(req.Body)

	var signUpRequest SignUpRequest
	if err := decoder.Decode(&signUpRequest); err != nil {
		api.RespondWithError(w, http.StatusBadRequest, api.ErrInvalidRequestPayload)
		return
	}

	defer req.Body.Close()

	// login verification
	if verification.LoginIsTooShort(signUpRequest.Login) {
		api.RespondWithError(w, http.StatusUnprocessableEntity, api.ErrLoginIsTooShort)
		return
	}

	// password verification
	if verification.PasswordIsTooShort(signUpRequest.Password) {
		api.RespondWithError(w, http.StatusUnprocessableEntity, api.ErrPasswordIsTooShort)
		return
	}

	// try to add a new user in the data base
	user, err := database.AddNewUser(signUpRequest.Login, signUpRequest.Password, database.Newbie, false)
	if err != nil {
		serverErr, ok := err.(*api.ServerError)
		if ok && serverErr.Code == api.ErrLoginIsAlreadyInUse {
			api.RespondWithError(w, http.StatusUnprocessableEntity, api.ErrLoginIsAlreadyInUse)
		} else {
			api.RespondWithInternalError(w, err.Error())
		}
		return
	}

	// create new jwt token
	accessToken, err := token.GenerateAccessToken(user.ID.String())
	if err != nil {
		api.RespondWithInternalError(w, err.Error())
		return
	}

	// create Response
	signUpResponse := SignUpResponse{
		data.User{
			Id:                 user.ID.String(),
			Login:              user.Login,
			UserType:           int(user.UserType),
			MustChangePassword: utils.Itob(user.MustChangePassword)},
		accessToken}

	api.RespondWithJSON(w, http.StatusCreated, signUpResponse)
}
