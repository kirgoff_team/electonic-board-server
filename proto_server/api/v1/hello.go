package api_v1

import (
	"net/http"
	"github.com/gorilla/mux"
	"encoding/json"
)

type helloData struct {
	HelloString string `json:"data"`
}

func Hello(w http.ResponseWriter, req *http.Request) {
	// if req.URL.Path != "/" {
	// 	log.Printf("404: %s", req.URL.String())
	// 	http.NotFound(w, req)

	// 	return
	// }

	vars := mux.Vars(req)
	w.WriteHeader(http.StatusOK)

	data := helloData{HelloString: vars["something"]}
	json.NewEncoder(w).Encode(data)

	//fmt.Fprintf(w, "Category: %v\n", vars["something"])
}