package api_v1

import (
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/token"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/database"
	"net/http"
)

type UsersResponse struct {
	Users []UserInfoResponse `json:"users"`
}

func GetUsers(w http.ResponseWriter, req *http.Request) {
	// get user id from token
	tokenString := req.Header.Get("Authorization")
	userId, err := token.GetUserIdFromAccessToken(tokenString)
	if err != nil {
		api.RespondWithError(w, http.StatusUnauthorized, api.ErrBadToken)
		return
	}

	// check user rights
	userType, err := database.GetUserType(userId)
	if err != nil {
		serverErr, ok := err.(*api.ServerError)
		if ok {
			switch serverErr.Code {
			case api.ErrUserNotFound:
				api.RespondWithError(w, http.StatusNotFound, api.ErrRequestingUserNotFound)
				return
			}
		}

		api.RespondWithInternalError(w, err.Error())
		return
	}

	if (database.Admin & userType) == 0 {
		api.RespondWithError(w, http.StatusForbidden, api.ErrPermissionDenied)
		return
	}

	usersArray, err := database.GetUsers(userId)

	if err != nil {
		api.RespondWithInternalError(w, err.Error())
	}

	users := make([]UserInfoResponse, 0)

	for i := 0; i < len(usersArray); i++ {
		userInfoResponse := UserInfoResponse{
			Id:       usersArray[i].ID.String(),
			Login:    usersArray[i].Login,
			UserType: int(usersArray[i].UserType)}

		users = append(users, userInfoResponse)
	}

	// create response
	usersResponse := UsersResponse{Users: users}

	api.RespondWithJSON(w, http.StatusOK, usersResponse)
}
