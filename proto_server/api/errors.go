package api

import (
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api/v1/verification"
	"strconv"
)

type ErrorCode int

const (
	// NOTE: don't change error codes for this error types, each new error types must have a new error code
	// Do not forget to add a description after adding a new error type

	ErrInvalidRequestPayload  ErrorCode = 100 /* Неправильный формат запроса */
	ErrUserTypeIsUnknown      ErrorCode = 101 /* Не известный тип пользователя */
	ErrBadToken               ErrorCode = 102 /* Плохой токен */
	ErrPermissionDenied       ErrorCode = 103 /* Доступ запрещён */
	ErrUserNotFound           ErrorCode = 104 /* Пользователь не найден */
	ErrRequestingUserNotFound ErrorCode = 105 /* Вопрошающий пользователь не найден */
	ErrLoginIsAlreadyInUse    ErrorCode = 106 /* Логин занят */
	ErrLoginIsTooShort        ErrorCode = 107 /* Логин слишком короткий */
	ErrPasswordIsTooShort     ErrorCode = 108 /* Пароль слишком короткий */
	ErrAuthorization          ErrorCode = 109 /* Ошибка авторизации, неверный логин или пароль */
	ErrInternal               ErrorCode = 500 /* Неизвестная ошибка */
)

type ServerError struct {
	Code ErrorCode
}

func (errorCode ErrorCode) String() string {

	switch errorCode {
	case ErrInvalidRequestPayload:
		return "Invalid request payload"
	case ErrUserTypeIsUnknown:
		return "User type is unknown"
	case ErrBadToken:
		return "Bad token"
	case ErrPermissionDenied:
		return "Permission denied"
	case ErrUserNotFound:
		return "User not found"
	case ErrRequestingUserNotFound:
		return "Requesting user not found"
	case ErrLoginIsAlreadyInUse:
		return "Login is already in use"
	case ErrLoginIsTooShort:
		return "Login is too short (min length is " + strconv.Itoa(verification.GetMinLoginLength()) + ")"
	case ErrPasswordIsTooShort:
		return "Password is too short (min length is " + strconv.Itoa(verification.GetMinPasswordLength()) + ")"
	case ErrAuthorization:
		return "Authorization error, login or password is incorrect"
	case ErrInternal:
		return "Internal server error"
	default:
		return "Unknown error"
	}
}

func (serverError ServerError) String() string {
	return serverError.Code.String()
}

func (serverError *ServerError) Error() string {
	return serverError.String()
}
