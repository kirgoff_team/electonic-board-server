package database

import (
	"log"
	"github.com/jinzhu/gorm"
)

// For compiling SQLite driver on Windows (64-bit), install gcc (64-bit) -> http://www.msys2.org/
// run "pacman -S mingw-w64-x86_64-toolchain" command in msys2

const (
	dbFileName = "proto.db"
	dbVersion  = 1
)

var db *gorm.DB

func Open() {
	// Open DataBase
	dataBase, err := gorm.Open("sqlite3", dbFileName)
	if err != nil {
		log.Fatal(err)
	}
	db = dataBase

	clear()
	create()
}

func create() {
	// If User table doesn't exist, create it
	if !db.HasTable(&User{}) {
		db.CreateTable(&User{})
		AddNewUser("admin", "12345", Admin, true)
	}
}

func clear() {
	db.DropTableIfExists(&User{})
}

func upgrade(oldVersion int, newVersion int) {

}

func Close()  {
	db.Close()
}