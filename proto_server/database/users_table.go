package database

import (
	"time"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/pborman/uuid"
	"github.com/mattn/go-sqlite3"
	"bitbucket.org/Samuel-Unknown/Kirgoff-Team/electonic-board-server/proto_server/api"
)

type UserType int

const (
	Newbie UserType = 1 << iota // Newbie == 1
	Reader                      // Reader == 2
	Admin                       // Admin  == 4

	// NOTE: don't change order of items, all new items must be added to the end
	// Newbie value used as default in User struct, don't change it

	// example of checking:
	//
	// adminAndReader := Admin | Reader
	//
	// if (adminAndReader & userType) != 0 {
	// 		// userType is Admin or Reader
	// }
)

func (userType UserType) String() string {

	switch userType {
	case Newbie:
		return "Newbie"
	case Reader:
		return "Reader"
	case Admin:
		return "Admin"
	default:
		return "Unknown"
	}
}

func (userType UserType) TypeIsUnknown() bool {
	return userType.String() == "Unknown"
}

type User struct {
	ID                 uuid.UUID `gorm:"primary_key"`           // blob in DB (is the same as sql.RawBytes and []byte)
	UserType           UserType  `gorm:"default:1"`             // int in DB (default value 1 corresponds to "Newbie" UserType)
	Login              string    `gorm:"not null;unique_index"` // varchar(255) in DB
	PasswordHash       string    `gorm:"not null"`              // varchar(255) in DB
	MustChangePassword int       `gorm:"default:0"`             // int in DB (1 - true, 0 - false)

	CreatedAt time.Time // datetime in DB
	UpdatedAt time.Time // datetime in DB
}

func (user *User) BeforeCreate(scope *gorm.Scope) error {
	scope.SetColumn("ID", uuid.NewRandom())
	return nil
}

func AddNewUser(login string, password string, userType UserType, mustChangePassword bool) (user *User, err error) {

	passwordHash, err := GenerateSaltedHash([]byte(password))
	if err != nil {
		return nil, err
	}

	if mustChangePassword {
		user = &User {Login: login, PasswordHash: passwordHash, UserType: userType, MustChangePassword: 1}
	} else {
		user = &User {Login: login, PasswordHash: passwordHash, UserType: userType, MustChangePassword: 0}
	}

	if err := db.Create(user).Error; err != nil {
		sqliteError, ok := err.(sqlite3.Error)
		if ok && sqliteError.Code == sqlite3.ErrConstraint {
			return nil, &api.ServerError{Code: api.ErrLoginIsAlreadyInUse}
		}

		return nil, err
	}

	return user, nil
}

func GetUser(login string, password string) (*User, error) {

	user := new(User)

	if db.Where(&User{Login: login}).First(user).RecordNotFound() {
		return nil, &api.ServerError{Code: api.ErrAuthorization}
	}

	if !PasswordIsOk(user.PasswordHash, password) {
		return nil, &api.ServerError{Code: api.ErrAuthorization}
	}

	return user, nil
}

func GetUserById(userId string) (*User, error) {
	user := new(User)

	if db.Where(&User{ID: uuid.Parse(userId)}).First(user).RecordNotFound() {
		return nil, &api.ServerError{Code: api.ErrUserNotFound}
	}

	return user, nil
}

func DeleteUserById(userId string) (error) {

	user := new(User)

	if db.Where(&User{ID: uuid.Parse(userId)}).First(user).RecordNotFound() {
		return &api.ServerError{Code: api.ErrUserNotFound}
	}

	if err := db.Delete(user).Error; err != nil {
		return err
	}

	return nil
}

func GetUsers(requestedUserId string) ([]User, error) {

	var users []User

	if err := db.Not(&User{ID: uuid.Parse(requestedUserId)}).Find(&users).Error; err != nil {
		return nil, err
	}

	return users, nil
}

func GetUserType(userId string) (UserType, error) {

	user, err := GetUserById(userId)

	if err != nil {
		return 0, err
	}

	return user.UserType, nil
}

func ChangeLogin(userID string, login string) (*User, error) {

	user := new(User)

	if db.Where(&User{ID: uuid.Parse(userID)}).First(user).RecordNotFound() {
		return nil, &api.ServerError{Code: api.ErrUserNotFound}
	}

	if err := db.Model(&user).Updates(User{Login: login}).Error; err != nil {
		sqliteError, ok := err.(sqlite3.Error)
		if ok && sqliteError.Code == sqlite3.ErrConstraint {
			return nil, &api.ServerError{Code: api.ErrLoginIsAlreadyInUse}
		}

		return nil, err
	}

	return user, nil
}

func ChangePassword(userID string, password string) (*User, error) {

	user := new(User)

	if db.Where(&User{ID: uuid.Parse(userID)}).First(user).RecordNotFound() {
		return nil, &api.ServerError{Code: api.ErrUserNotFound}
	}

	passwordHash, err := GenerateSaltedHash([]byte(password))
	if err != nil {
		return nil, err
	}

	if err := db.Model(&user).Updates(User{PasswordHash: passwordHash}).Error; err != nil {
		return nil, err
	}

	return user, nil
}

func ChangeUserType(userID string, userType UserType) (*User, error) {
	user := new(User)

	if db.Where(&User{ID: uuid.Parse(userID)}).First(user).RecordNotFound() {
		return nil, &api.ServerError{Code: api.ErrUserNotFound}
	}

	if err := db.Model(&user).Updates(User{UserType: userType}).Error; err != nil {
		return nil, err
	}

	return user, nil
}