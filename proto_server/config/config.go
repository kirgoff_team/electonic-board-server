package config

const (
	// Proto Server Version
	versionMajor = "1" // MAJOR version when you make incompatible API changes
	versionMinor = "0" // MINOR version when you add functionality in a backwards-compatible manner
	versionPatch = "0" // PATCH version when you make backwards-compatible bug fixes

	VersionName  = versionMajor + "." + versionMinor + "." + versionPatch
)